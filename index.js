
//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const cors = require("cors");
  const productRoutes = require('./routes/product'); 
  const userRoutes = require('./routes/user');
  const orderRoutes = require('./routes/order');
  const cartRoutes = require('./routes/cart');
  const categoryRoutes = require('./routes/category');

//[SECTION] Server Setup
  const app = express(); 
  app.use(cors())
  app.use(express.json());
  app.use(express.urlencoded({extended:true}));


//[SECTION] Application Routes
  app.use('/users',userRoutes); 
  app.use('/products',productRoutes);
  app.use('/orders', orderRoutes);
  app.use('/carts', cartRoutes);
  app.use('/categories', categoryRoutes);


//[SECTION] Database Connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.55eun.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority",{
  useNewUrlParser: true,
  useUnifiedTopology: true

});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

//[SECTION] Gateway Response
   
app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000 }`)
}) 
