const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({
    user: { 
      type: new mongoose.Schema({
            userId:{
              type:String,
              required: [true, "userId is required"]
            },
          fullName : {
            type:String,
            required: [true, "firstname name is required"]
          },
          email: {
            type: String,
            required: [true, "Email is required"],
               index: true,
               sparse: true
          },
      }),
      required: true
    },
    products: [{
      type: new mongoose.Schema({
          productId: {
          type: String,
          required: true
        },
          name: {
          type: String
        },
          price: {
          type: Number
        },
          quantity: {
          type: Number,
          default: 1
        },
      }),
    }],
    totalAmount: {
     type: Number,
     min: 0 
    },
    paymentStatus: { 
      type: String, 
      default: "pending" 
    },
    orderStatus: { 
      type: String, 
      default: "ordered" 
    },
    purchasedOn: {
      type: Date,
      default: new Date()
      }
});


module.exports = mongoose.model('Orders', orderSchema);
