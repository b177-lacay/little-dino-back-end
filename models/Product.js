// const Joi = require('joi');
const mongoose = require('mongoose');
// const {categorySchema} = require('./Category')


const productSchema = new mongoose.Schema({
      name: {
        type: String,
        required: true
      },
      category: {
        type: new mongoose.Schema({
            categoryId: {
                type: String,
                requred:true
            },
            categoryName: {
              type:String
            },
        }),
        required: true
      },
      description: {
      	type: String,
      	required: true,
      },
      img: {
        type: String,
        required: true
      },
       price: {
        type: Number,
        required: true
      },
      numberOfStocks:
      {
        type: Number
      },
      isAvailable: {
          type: Boolean,
          default: true
      },
      createdOn: {
      type: Date,
      default: new Date()
      }
});

module.exports = mongoose.model('Product', productSchema);
