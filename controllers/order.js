const Product = require('../models/Product');
const User = require('../models/User');
const Order = require('../models/Orders');
const auth = require('../auth');

// [SECTION- add order]
exports.addOrder = async(req,res) => {

const data = auth.decode(req.headers.authorization);

if(data.isAdmin) return res.status(401).send('Only regular users are allowed to order!');
	try{
		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!');


			let newOrder = new Order({
			user: {
			userId:user._id,
			fullName: user.fullName,
			email: user.email,
				  }
			});

		const product = await Product.findById(req.body.productId);
		if(!product) return res.status(400).send('Invalid product!');

			if(req.body.quantity == undefined)
				req.body.quantity = 1;
			if((product.numberOfStocks !== 0) && (req.body.quantity <= product.numberOfStocks)){

				newOrder.products.push({
				productId: product._id,
				name: product.name,
				price: product.price,
				quantity: req.body.quantity
				})
				

				let newOrderQuantity = newOrder.products[0].quantity;
				

				newOrder.totalAmount = product.price*newOrderQuantity;
				product.numberOfStocks-= newOrderQuantity;
				if(product.numberOfStocks === 0){
					product.isAvailable = false;
					await product.save();

					newOrder = await newOrder.save();
					return res.send(newOrder);

				} else {
					await product.save();

					newOrder = await newOrder.save();
					return res.send(newOrder);
				}
				
			}else {
				console.log(req.body.quantity);
				if(product.numberOfStocks === 0)
				return res.status(400).send(`${product.name} is currently out of stock!`);
				return res.status(400).send(`Sorry, we have ${product.numberOfStocks} stocks available for this item!`)
			}

		}catch(err){
			return res.status(500).send(err.message);
		}

}



// [SECTION- get all order]

exports.getAllOrders = async(req,res) => {

const data = auth.decode(req.headers.authorization);

if(!data.isAdmin) return res.status(401).send('Unauthorized user!');

	try{

		const orders = await Order.find({});

		if(orders == "") return res.status(404).send('Empty result for orders.');

		return res.send(orders);

	}catch(err){

		res.status(500).send(err.message);

		}
}



// [SECTION- get user's order]

exports.getUserOrders = async(req,res) => {

const data = auth.decode(req.headers.authorization);
req.params.userId = data.id;
if(data.isAdmin) return res.status(401).send('Only regular users are allowed!');

	try{

		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!');

		const order = await Order.find({"user.userId": data.id});

		if(order == "") return res.status(404).send('You have no orders.');

		return res.send(order);	

	}catch(err){
		return res.status(500).send(err.message);
	}

}


// [SECTION- get order]
exports.testOrder = async(req,res) => {
const data = auth.decode(req.headers.authorization);

if(data.isAdmin) return res.status(401).send('Only regular users are allowed to order!');
	try{
		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!');


			let newOrder = new Order({
			user: {
			userId:user._id,
			fullName: user.fullName,
			email: user.email
				  },
			products: [{
				name: req.body.products.name,
				price: req.body.products.price,
				quantity: req.body.products.quantity
				}],
			totalAmount: req.body.totalAmount  
			});

let allProducts = [];
let products = newOrder.products;

// Populate users array
for(let key in products) {
    allProducts.push(products[key]);
}

	newOrder = await newOrder.save();
	return res.send(newOrder);

	}catch(err){

		return res.status(500).send(err.message);

	}

}		

