const User = require('../models/User');
const validate = require('../models/validator');
const bcrypt = require('bcrypt');
const auth = require('../auth')



//*************************************Registration*************************************//
exports.registerUser = async(req,res) => {

	let isEmailTaken = await User.findOne({"email": req.body.email})
	 	if(isEmailTaken) return res.status(400).send('Email already in use!')

const {error} = validate.validateUser(req.body);

if (error) return res.status(400).send(error.details[0].message);

			let newUser = new User({

			fullName: req.body.fullName,

			email: req.body.email,

			password: bcrypt.hashSync(req.body.password,10),

			confirmPassword: req.body.confirmPassword
		})


			newUser.confirmPassword = newUser.password;

			newUser.save()

			.then(() => res.send(newUser))

			.catch(err => res.status(500).send(err.message));

}




//******************************************login**************************************//
exports.loginUser = (req,res) => {

	const { error } = validate.validateLogin(req.body); 

	if (error) return res.status(400).send(error.details[0].message);

	 User.findOne({email: req.body.email})
	.then(result => {
		if(!result)  return res.status(400).send('Invalid email!');

		const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

		if(!isPasswordCorrect) return res.status(400).send('Password is incorrect!');

		let token = auth.createAccessToken(result);
		return res.send(token);

	})
	.catch(err => res.status(500).send(err.message));
}





//******************************************SET Admin**************************************//

 exports.setAdmin = async(req,res) => {
 	
 	const data = auth.decode(req.headers.authorization);

 	if(!data.isAdmin) return res.status(401).send('Unauthorized user!');

 	const isAlreadyAdmin = await User.findById(req.body.userId);
 	if(!isAlreadyAdmin) return res.status(404).send('User does not exist!')
 	if(isAlreadyAdmin.isAdmin) return res.status(400).send(`${isAlreadyAdmin.fullName} is already an Admin!`);

 	User.findById(req.body.userId)
 		.then(result => {
 			if(!result) return res.status(400).send('Invaid user!');

 			result.isAdmin = true;

 			result.save()
 			.then(result => res.send(`${result.fullName} is now an Admin!`))
 		})
 		.catch(err => res.status(500).send(err.message));
}


//******************************************Get user profile**************************************//
	module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

	
		result.password = "";

		
		return result;

	});

};

//********************* retrieving all isers (admin only)********************//
exports.getAllUsers = async(req,res) => {

	try{
		const data = auth.decode(req.headers.authorization);

		if(!data.isAdmin) return res.status(401).send(`Unauthorized user!`);

		const user = await User.find({})

		res.send(user)

	}catch(err){
		res.send(err)
	}	

}

	
	



	


