const Product = require('../models/Product');
const Category = require('../models/Category');
const auth = require('../auth');
// const validate = require('../models/validator');


//*******************************Add a product**************************************//

exports.addProduct = async(req,res) => {
	
	const data = auth.decode(req.headers.authorization);

	if (!data.isAdmin) return res.status(401).send('Unauthorized user!');

	// const { error } = validate.validateCourse(req.body); 
	// if (error) return res.status(400).send(error.details[0].message);

	try{

		let isNameTaken = await Product.findOne({"name": req.body.name})
		 	if(isNameTaken) return res.status(400).send('Product name is already in use!')
		 

		const category = await Category.findById(req.body.categoryId);
	  	if (!category) return res.status(400).send('Invalid category!');

			let newProduct = new Product({
				name : req.body.name,
				category: {
					categoryId: category._id,
					categoryName: category.name
				},
				description : req.body.description,
				img: req.body.img,
				price: req.body.price,
				numberOfStocks: req.body.numberOfStocks
			});
		
			newProduct = await newProduct.save();
			return res.send(`Product successfully added!
				${newProduct}`);

	}catch(err){

		return res.status(500).send(err.message);

	}
}


//********************* retrieving all products (admin only)********************//
exports.getAllProducts = async(req,res) => {

	const data = auth.decode(req.headers.authorization);

	if(!data.isAdmin) return res.status(401).send(`Unauthorized user!`);

	Product.find({}).then(result => {
		
	return res.send(result);

	})
	.catch(err => res.status(500).send(err.message));
}



//********************* retrieval of available products *******************//
exports.getAllAvailableProducts = (req,res) => {

	
	Product.find({isAvailable: true})
			.then(product => {
				return res.send(product);
			})
			.catch(err => res.status(500).send(err.message));
	
}



//*************************retrieval of available products by tags*********************//
exports.getAllAvailableByTags = (req,res) => {

const queryTags = req.query.tags;

	Product.find()

	.and([{tags: {$in: [queryTags]}},{isAvailable: true}])

	.then(product => {
			if(product == '') return res.status(404).send('Empty search result!')
			return res.send(product);
	})

	.catch(err => res.status(500).send(err.message));

}



//********************retrieval of a specific product**********************//

exports.getProduct = (req,res) => {
	Product.findById(req.params.productId)
		.then(result => {
			if(!result) return res.status(400).send('Invalid productId!');
			return res.send(result);
		})
		.catch(err => res.status(500).send(err.message))
}


//**********************updating a product*************************//

exports.updateProduct = async(req,res) => {

	const product = await Product.find({});
	
	const data = auth.decode(req.headers.authorization);
	if(!data.isAdmin) return res.status(401).send('Unauthorized user!');

	Product.findById(req.params.productId)
		.then(result => {
			if(!result) return res.status(400).send('Invalid productId!')
				result.name = req.body.name
				result.description = req.body.description
				result.tags = req.body.tags
				result.price = req.body.price
				result.numberOfStocks = req.body.numberOfStocks
				

				for(let i = 0; i < product.length; i++){
					if(req.params.productId == product[i]._id) continue;
					if(req.body.name == product[i].name) return res.status(400).send('Name already in use!')	
				}

			return result.save()
				.then(updatedProduct => {
					return res.send(`Product successfully updated!
						${updatedProduct}`)
				})

	})	
		.catch(err => res.status(500).send(err.message));
}




//***********************archiving a product************************//

exports.archiveProduct = (req,res) => {
	
	const data = auth.decode(req.headers.authorization);
	

	if(!data.isAdmin) return res.status(400).send('Unauthorized user!')


	Product.findById(req.body.productId)
		.then(result => {

			if(!result.isAvailable) return res.status(400).send(`${result.name} is already archived!`)
			result.isAvailable = false;


			result.save()
			.then(archivedProduct => {

			return res.send(`${archivedProduct.name} is successfully archived!`);
	
			})

	} )
		.catch(err => res.status(500).send(err.message));
}







