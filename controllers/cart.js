const Cart = require('../models/Cart');
const Product = require('../models/Product');
const User = require('../models/User');
const Order = require('../models/Orders');
const auth = require('../auth');


// [SECTION- Add to cart]

exports.addCart = async(req,res) => {

data = auth.decode(req.headers.authorization)

if(data.isAdmin) return res.status(401).send('Only regular users are allowed to add to cart.')

	try {

		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!')

		const cart = await Cart.findOne({"user.userId": data.id});
		 
			if(!cart){

				const product = await Product.findById(req.body.productId);
				
				if(!product) return res.status(400).send('Invalid product!');
				if(!product.isAvailable) return res.status(400).send(`${product.name} is currently out of stock.`);


								let addToCart = new Cart({
								user: {
								userId: user._id,
								email: user.email,
									  }
							})

								addToCart.totalAmount += product.price;
								addToCart.products.push({
									productId: product._id,
									name: product.name,
									price: product.price
								})

							 let addedCart = await addToCart.save();

							 res.send(addedCart);
									
			} else {

				const cart = await Cart.findOne({"user.userId": data.id});

				const product = await Product.findById(req.body.productId);

				if(!product) return res.stats(400).send('Invalid product!');

				let products = cart.products;

				const pro = products.find( ({ productId }) => productId === req.body.productId );
				if(pro) {

				if(pro.quantity === product.numberOfStocks) return res.status(400).send(`All ${product.numberOfStocks} ${product.name} are in your cart!`)

				if(product.isAvailable = false) return res.status(400).send(`${product.name} is currently out of stock.`);

				pro.quantity++

				cart.totalAmount += pro.price

				let cartSave = await cart.save();
				return res.send(cartSave);
				} else {

					cart.products.push({
					productId: product._id,
					name: product.name,
					price: product.price
					})

					cart.totalAmount += product.price;
								
					let savedCart =	await cart.save();
					return res.send(savedCart);
				}						
			}

	}catch(err){

		return res.status(500).send(err.message);

	}
}


// [SECTION- checkout]


exports.addCheckout = async(req,res) => {

const data = auth.decode(req.headers.authorization);

req.params.userId = data.id;

if(data.isAdmin) return res.status(401).send('Only regular users are allowed for this transaction.')

	try{

		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!');

		const cart = await Cart.findOne({"user.userId": data.id});
					
		if(!cart) return res.status(404).send('Your cart is Empty.');

			let newOrder = new Order({
			user: {
			userId: user._id,
			fullName: user.fullName,
			email: user.email
			   	  }
			});

		let cartProducts = cart.products;
		let productNoOfStocks = [];
		let insuProductStocks = [];
		let product;
		let productCollection = [];

			for (let i = 0; i < cartProducts.length; i++) {

				newOrder.products.push(cartProducts[i]);

				product = await Product.findById(cartProducts[i].productId);
				
				if(!product) return res.status(400).send('Invalid product!')

				product.numberOfStocks-=cartProducts[i].quantity;

				productNoOfStocks.push(product.numberOfStocks);

				productCollection.push(product);

				if(product.numberOfStocks < 0) 

				insuProductStocks.push(product.name);

					
			}
			
				let isNotNegative = productNoOfStocks.every( e  => e > -1);
				
				if(isNotNegative){
					

					for(let i = 0; i < productNoOfStocks.length; i++ ){

						product = await Product.findById(productCollection[i]._id);

						if(productNoOfStocks[i] == 0){
							product.numberOfStocks = productNoOfStocks[i];
							product.isAvailable = false;

							await product.save();
						

						} else {
							product.numberOfStocks = productNoOfStocks[i];
							await product.save();
						}

					}


					newOrder.totalAmount = cart.totalAmount;

					newOrder = await newOrder.save();

					await Cart.deleteOne({"user.userId": req.params.userId});

					return res.send(newOrder);

				} else {
					
					return res.status(400).send(`Checkout failed! , insufficient stocks for the following items: ${insuProductStocks}.
						Please update your cart.`)
				}

	}catch(err){

		return res.status(500).send(err.message);

	}

}



// [SECTION- Get all cart]

exports.getAllCarts = async(req,res) => {

const data = auth.decode(req.headers.authorization);

if(!data.isAdmin) return res.send('Unauthorized user.');

	try{

		const carts = await Cart.find({});

		if(carts == "") return res.send('Empty result for carts.');

		return res.send(carts);

	}catch(err){

		res.status(500).send(err.message);

		}
}



// [SECTION- get user cart]

exports.getUserCart = async(req,res) => {

const data = auth.decode(req.headers.authorization);
if(data.isAdmin) return res.send('Only regular users are allowed.');
// req.params.userId = data.id;

// if(data.id !== req.params.userId) return res.send('Invalid user');

	try{

		const user = await User.findById(data.id);

		if(!user) return res.status(400).send('Invalid user!');

		const cart = await Cart.find({"user.userId": data.id});

		if(cart == "") return res.status(404).send('Your cart is empty!');

		return res.send(cart);	

	}catch(err){
		return res.status(500).send(err.message);
	}

}


// [SECTION- cart increment]

exports.incrementCart = async(req,res) => {

try{
	const data = auth.decode(req.headers.authorization);
	if(data.isAdmin) return res.send('Only regular users are allowed.');


	const user = await User.findById(data.id);
	const product = await Product.findById(req.body.productId);
	const cart = await Cart.findOne({"user.userId": data.id});

	for(let i = 0; i < cart?.products?.length; i++)

		if(cart.products[i].productId == req.body.productId){
			console.log('test')

			if(product.numberOfStocks == cart.products[i].quantity)
				return res.status(400).send(false)

			cart.products[i].quantity += 1
			cart.totalAmount += cart.products[i].price
			await cart.save();		
			return res.send(cart.products);


		}else{
			console.log('failed')
			continue;
		}

		
}catch(err){
	return res.status(500).send(err.message);
}

}


// [SECTION- decrement cart]

exports.decrementCart = async(req,res) => {

	// const cart = await Cart.find({});
	try{
		const data = auth.decode(req.headers.authorization);
		if(data.isAdmin) return res.send('Only regular users are allowed.');


		const user = await User.findById(data.id);


		const cart = await Cart.findOne({"user.userId": data.id});





		for(let i = 0; i < cart?.products?.length; i++)

		if(cart.products[i].productId == req.body.productId){


				
			if(cart.products[i].quantity > 1){

		cart.products[i].quantity -= 1
		cart.totalAmount -= cart.products[i].price
		await cart.save();		
		return res.send(cart.products);

			} else if(cart.products.length > 1){
				cart.totalAmount -= cart.products[i].price
				cart.products.splice([i],1)
				await cart.save();		
				return res.send(cart.products)
			} else{
				await Cart.deleteOne({"user.userId": data.id});
				return res.send('Your Cart is empty.');
			}

		}else{
				
		continue;
		
		}


	}catch(err){
		return res.status(500).send(err.message);
	}

}


// [SECTION- delete cart]

exports.deleteCart = async(req,res) => {

	try{
		const data = auth.decode(req.headers.authorization);
		if(data.isAdmin) return res.send('Only regular users are allowed.');


		const user = await User.findById(data.id);


				await Cart.deleteOne({"user.userId": data.id});

				return res.send('Your Cart is empty.');

	}catch(err){
		return res.status(500).send(err.message);
	}

}


// [SECTION- remove item cart]

exports.removeCartItem = async(req,res) => {
	try{
		const data = auth.decode(req.headers.authorization);
		if(data.isAdmin) return res.send('Only regular users are allowed.');


		const user = await User.findById(data.id);



		const cart = await Cart.findOne({"user.userId": data.id});


		for(let i = 0; i < cart?.products?.length; i++){

			if(cart.products[i].productId == req.body.productId){

				if(cart?.products?.length > 1){

					cart.totalAmount -= cart.products[i].quantity*cart.products[i].price
					cart.products.splice([i],1)

					await cart.save();
					return res.send(cart.products)

				} else{
					await Cart.deleteOne({"user.userId": data.id});

					return res.send('Your Cart is empty.');
					}	
			}else{
				
				continue;
		
			}

		}


	}catch(err){
		return res.status(500).send(err.message);
	}
}
