const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')
const auth = require('../auth');



// Registration
router.post('/register', userController.registerUser);

//Login
router.post('/login', userController.loginUser);

//setAdmin
router.post('/setAdmin', auth.verify, userController.setAdmin);


// Retrieve specific user details
router.post("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
        
    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


//get all users by admin
router.get('/all', auth.verify, userController.getAllUsers);



module.exports = router;