const express = require('express');
const router = express.Router();
const categoryController = require('../controllers/category')
const auth = require('../auth');


//Create a category
router.post('/', auth.verify, categoryController.addCategory);

// Get all Categories
router.get('/', categoryController.getAllCategories);

// Delete a category
router.delete('/:id', auth.verify, categoryController.deleteCategory);


// retrieval of specific category
router.get('/:id', categoryController.getCategory);


// getting products by category

router.get('/products/:categoryId', categoryController.getProductsByCategory)



module.exports = router;



